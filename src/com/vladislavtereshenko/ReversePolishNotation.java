package com.vladislavtereshenko;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ReversePolishNotation {

    private String polish;
    private static Map<Character,Integer> symbols = new HashMap<Character,Integer>(){{
        put('(',0);
        put(')',0);
        put('-',1);
        put('+',1);
        put('*',2);
        put('/',2);
    }};
    private static Map<Character,Integer> logicsymbols = new HashMap<Character,Integer>(){{
        put('(',0);
        put(')',0);
        put('!',1);
        put('&',2);
        put('|',3);
        put('→',4);
        put('↔',4);
    }};
    private Stack<Character> datasymbols = new Stack<Character>();
    private ReversePolishNotation(String data){
        this.polish = data;
    }
    private String makeNotationForLogic(){
        String data = "";
        for(Character i : polish.toCharArray()){
            if(!logicsymbols.containsKey(i)){
                data+=i;
            }
            else{
                if(!datasymbols.empty() && !i.equals('(')){
                    if(i.equals(')')){
                        char temp = datasymbols.pop();
                        while(temp!='('){
                            data+=temp;
                            temp = datasymbols.pop();
                        }
                    }
                    else if(logicsymbols.get(i)>=logicsymbols.get(datasymbols.peek()))
                        datasymbols.push(i);
                    else{
                        while(!datasymbols.empty() && logicsymbols.get(i)<logicsymbols.get(datasymbols.peek())){
                            data+=datasymbols.pop();
                        }
                        datasymbols.push(i);
                    }
                }
                else {
                    datasymbols.push(i);
                }
            }
        }
        if(!datasymbols.empty()){
            for(int i = datasymbols.size()-1; i >= 0;i--){
                data+=datasymbols.elementAt(i);
            }
        }
        datasymbols.clear();
        return data;
    }
    private String makeNotation(){
        String data = "";
        for(Character i : polish.toCharArray()){
            if(!symbols.containsKey(i)){
                data+=i;
            }
            else{
                if(!datasymbols.empty() && !i.equals('(')){
                    if(i.equals(')')){
                        char temp = datasymbols.pop();
                        while(temp!='('){
                            data+=temp;
                            temp = datasymbols.pop();
                        }
                    }
                    else if(symbols.get(i)>=symbols.get(datasymbols.peek()))
                        datasymbols.push(i);
                    else{
                        while(!datasymbols.empty() && symbols.get(i)<symbols.get(datasymbols.peek())){
                            data+=datasymbols.pop();
                        }
                        datasymbols.push(i);
                    }
                }
                else {
                    datasymbols.push(i);
                }
            }
        }
        if(!datasymbols.empty()){
            for(int i = datasymbols.size()-1; i >= 0;i--){
                data+=datasymbols.elementAt(i);
            }
        }
        datasymbols.clear();
        return data;
    }
    public static void main(String[] args) {
	// write your code here
        ReversePolishNotation a = new ReversePolishNotation("(a+b)*c-d/(e+a*f)");
        ReversePolishNotation b = new ReversePolishNotation("(a+b)*c");
        System.out.println(new ReversePolishNotation("x=a&b-c").makeNotation());
        System.out.println(a.makeNotation());
        System.out.println(b.makeNotation());
        System.out.println(new ReversePolishNotation("a&b").makeNotationForLogic());
        System.out.println((new ReversePolishNotation("(1|a)&b").makeNotationForLogic()));
        System.out.println(new ReversePolishNotation("a=(c&b)|x").makeNotationForLogic());
        System.out.println(new ReversePolishNotation("a[i]=b&c").makeNotationForLogic());
    }
}
